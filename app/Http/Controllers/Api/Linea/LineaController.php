<?php

namespace App\Http\Controllers\Api\Linea;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use Illuminate\Http\Request;



class LineaController extends Controller
{
    public function getLineas(Request $request)
    {
        try {
            // $lineas = Linea::get();
            $valid = false;

            $linea = Linea::select('lineas.*');

            if ($request->rol != 1) {
                $linea->join('usuario_proyecto', 'usuario_proyecto.id_proyecto', '=', 'lineas.proyecto')
                    ->join('usuario', 'usuario.id', '=', 'usuario_proyecto.id_usuario')
                    ->where('usuario.id', $request->id);
            }
            if ($request->proyecto['id'] != 0) {
                $linea->where('lineas.proyecto',strval($request->proyecto['id']));
            }

            return response()->json([
                "data" => $linea->orderBy('lineas.consumo_mb', 'desc')->get()
            ]);
            //code...
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                "data" => [],
                "error" => $e->getMessage()
            ]);
        }
    }
    public function lineasConsumo(Request $request)
    {

        $consumo = LineaConsumo::where('linea', $request->id_linea)->get();

        return response()->json([
            "consumo" => $consumo
        ]);
    }
    public function getSitio(Request $request)
    {
        $sitio = Sitio::where('id_sitio', $request->id_sitio)
        ->first();
        return response()->json([
            "sitio" => $sitio
        ]);
    }
    public function getSitios(Request $request)
    {
        $sitios = Sitio::select('*');

        if($request->proyecto_id != 0){
            $sitios->where('proyecto',$request->proyecto_id);
        }
        return response()->json([
            "sitios" => $sitios->get()
        ]);
    }

    public function changeSitio(Request $request){
        $linea = Linea::where('id',$request->id)
        ->where('linea',$request->linea)
        ->first();
        $linea->id_sitio = $request->id_sitio;
        $linea->save();

        return response()->json([
            "success"=>true,
        ]);
    }

    public function prueba()
    {
        return "prueba";
    }
}
