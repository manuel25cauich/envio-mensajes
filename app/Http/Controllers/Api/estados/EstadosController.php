<?php

namespace App\Http\Controllers\Api\estados;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Estados;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EstadosController extends Controller
{
    public function guardarEstados(Request $request)
    {
        try {
            DB::beginTransaction();
            foreach ($request->estados as $estado) {
                $estadoN = new Estados();
                // $estadoN->d_codigo = $estado['d_codigo'];
                $estadoN->D_mnpio = $estado['D_mnpio'];
                $estadoN->c_CP = $estado['c_CP'];
                $estadoN->c_cve_ciudad = $estado['c_cve_ciudad'];
                $estadoN->c_estado = $estado['c_estado'];
                $estadoN->c_mnpio = $estado['c_mnpio'];
                $estadoN->c_oficina = $estado['c_oficina'];
                $estadoN->c_tipo_asenta = $estado['c_tipo_asenta'];
                $estadoN->d_CP = $estado['d_CP'];
                $estadoN->d_asenta = $estado['d_asenta'];
                $estadoN->d_ciudad = $estado['d_ciudad'];
                $estadoN->d_codigo = $estado['d_codigo'];
                $estadoN->d_estado = $estado['d_estado'];
                $estadoN->d_tipo_asenta = $estado['d_tipo_asenta'];
                $estadoN->d_zona = $estado['d_zona'];
                $estadoN->id_asenta_cpcons = $estado['id_asenta_cpcons'];

                $estadoN->save();
            }
            DB::commit();
            // $lineas = Linea::get();
            return response()->json([
                "success" => true
            ]);
            //code...
        } catch (Exception $e) {
            DB::rollBack();
            //throw $th;
            return response()->json([
                "success" => false,
                "error" => $e->getMessage()
            ]);
        }
    }
    public function getEstado(Request $request){
        $d = Estados::select('d_asenta')->where('d_codigo','LIKE','%'.$request->cp)->groupBy('d_asenta')->get();
        $estados = Estados::select('d_estado')->where('d_codigo','LIKE','%'.$request->cp)->groupBy('d_estado')->get();
        $municipios = Estados::select('D_mnpio')->where('d_codigo','LIKE','%'.$request->cp)->groupBy('D_mnpio')->get();

        return response()->json([
            "estados"=>$d,
            "optionsEstado"=>$estados,
            "optionsMunicipios"=>$municipios,
        ]);
    }
}
