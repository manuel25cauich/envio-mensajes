<?php

namespace App\Http\Controllers\Api\monitoreored;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Alarma;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use Illuminate\Http\Request;



class MonitoreoRedController extends Controller
{
    public function getAlarma(Request $request)
    {
        try {
            $estado = $request->estado == 1? 'S':'N';

            // $lineas = Linea::get();
            $valid = false;

            $alarma = Alarma::select('alarmas.*');

            if($request->proyecto != 0){
                $alarma->join('lineas','lineas.linea','alarmas.linea')
                ->where('lineas.proyecto',$request->proyecto);
            }

            if($request->estado != 3){
                $alarma->where('alarmas.closed',$estado);
            }



            return response()->json([
                "data" => $alarma->get()
            ]);
            //code...
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                "data" => [],
                "error" => $e->getMessage()
            ]);
        }
    }

}
