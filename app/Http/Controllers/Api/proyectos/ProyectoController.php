<?php

namespace App\Http\Controllers\Api\proyectos;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use Illuminate\Http\Request;



class ProyectoController extends Controller
{
    public function getProyectos(Request $request)
    {

        try {
            if ($request->rol != 1) {
                $proyectos = ProyectoUsuario::select('proyecto.*')->join('proyecto', 'proyecto.id', '=', 'usuario_proyecto.id_proyecto')
                    ->where('usuario_proyecto.id_usuario', $request->id)->get();
            } else {
                $proyectos = Proyecto::get();
            }


            return response()->json([
                "data" => $proyectos
            ]);
            //code...
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                "data" => [],
                "error" => $e->getMessage()
            ]);
        }
    }
    public function prueba()
    {
        return "prueba";
    }
}
