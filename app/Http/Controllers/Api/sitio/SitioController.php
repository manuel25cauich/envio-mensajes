<?php

namespace App\Http\Controllers\Api\sitio;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use Illuminate\Http\Request;



class SitioController extends Controller
{
    public function getSitiosUser(Request $request)
    {
        $sitio = Sitio::with('proyecto')->select('*');

        if($request->proyecto['id'] != 0){
            $sitio->where('proyecto', $request->proyecto['id']);
        }

        return response()->json([
            "sitios"=>$sitio->get()
        ]);
    }

    public function guardarSitio(Request $request){
        $idSitio = Sitio::get();
        $countId = count($idSitio);


        try {
            if($request->id != ''){
                $countId = $request->id_sitio;
                $sitio = Sitio::where('id',$request->id)->first();
            }else{
                $sitio = new Sitio();
            }
            // return response()->json([
            //     "countId"=>$countId
            // ]);

            $sitio->id_sitio = strval($countId);
            $sitio->nombre_sitio = $request->nombre_sitio;
            $sitio->calle_sitio = $request->calle_sitio;
            $sitio->numero_sitio = $request->numero_sitio;
            $sitio->colonia_sitio = $request->colonia_sitio['d_asenta']??$request->colonia_sitio;
            $sitio->municipio_sitio = $request->municipio_sitio['D_mnpio'] ??  $request->municipio_sitio;
            $sitio->estado_sitio = $request->estado_sitio['d_estado'] ?? $request->estado_sitio;
            $sitio->cp_sitio = $request->cp_sitio;
            $sitio->latitud_sitio = $request->latitud_sitio;
            $sitio->longitud_sitio = $request->longitud_sitio;
            $sitio->proyecto = $request->proyecto['id'];
            $sitio->save();

            return response()->json([
                "success"=>true
            ]);
        } catch (Exception $e) {
            return response()->json([
                "success"=>false,
                "msg"=>$e->getMessage(),
                "countId"=>$countId
            ]);
        }
    }
}
