<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sitio extends Model
{
    protected $table = "sitios";
    public function proyecto(){
        return $this->hasOne(Proyecto::class,"id","proyecto");
    }
}
