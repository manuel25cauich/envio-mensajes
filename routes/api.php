<?php

use App\Http\Controllers\Api\estados\EstadosController;
use App\Http\Controllers\Api\Linea\LineaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Login\LoginController;
use App\Http\Controllers\Api\monitoreored\MonitoreoRedController;
use App\Http\Controllers\Api\proyectos\ProyectoController;
use App\Http\Controllers\Api\sitio\SitioController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [LoginController::class, 'login']);


// Linea
Route::post('getLinea', [LineaController::class, 'getLineas']);
Route::post('getConsumo', [LineaController::class, 'lineasConsumo']);
Route::post('getSitio', [LineaController::class, 'getSitio']);
Route::post('getSitios', [LineaController::class, 'getSitios']);
Route::post('changeSitio', [LineaController::class, 'changeSitio']);

//Proyectos
Route::post('getProyecto', [ProyectoController::class, 'getProyectos']);

//Monitoreo de red
Route::post('getAlarma', [MonitoreoRedController::class, 'getAlarma']);

//sitio
Route::post('getSitiosUser', [SitioController::class, 'getSitiosUser']);
Route::post('guardarSitio', [SitioController::class, 'guardarSitio']);

//estados
Route::post('guardarEstados', [EstadosController::class, 'guardarEstados']);
Route::post('getEstado', [EstadosController::class, 'getEstado']);


